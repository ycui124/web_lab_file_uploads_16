package ictgradschool.web.lab.uploads;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class FileUploads extends HttpServlet {

    //TODO - make this a proper HttpServlet that uses init() and doPost() methods

    private File uploadFolder;
    private File tempFolder;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setSizeThreshold(4 * 1024);
        factory.setRepository(tempFolder);
        ServletFileUpload fileUpload = new ServletFileUpload(factory);

        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();

        try {
            List<FileItem> fileItems = fileUpload.parseRequest(req);
            File imgFile = null;

            for (FileItem fi : fileItems) {
                if (!fi.isFormField()){// && (fi.getContentType().equals("image/png") || fi.getContentType().equals("image/jpeg"))) {
                    System.out.println(fi.getName());
                    String filename = fi.getName();
                    imgFile = new File(uploadFolder, filename);
                    fi.write(imgFile);
                }
            }
            if(imgFile==null){
                System.out.println("imgFile is null.");
            }else{
                out.println("<img src=/UploadedPhotos/" + imgFile.getName() + " width=\"200\">");
            }
        } catch (Exception e) {
//            e.printStackTrace();
            throw new ServletException(e);
        }
    }

    @Override
    public void init() throws ServletException {
        super.init();
        this.uploadFolder = new File(getServletContext().getRealPath("/UploadedPhotos"));
        if (!uploadFolder.exists()) {
            uploadFolder.mkdirs();
        }
        this.tempFolder = new File(getServletContext().getRealPath("/WEB-INF/tempFolder"));
        if (!tempFolder.exists()) {
            tempFolder.mkdirs();
        }
    }
}
